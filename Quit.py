from KeyEvent import *
import pygame

class Quit:
	"""..."""
	def __init__(self, evManager, display):
		self.evManager = evManager
		self.evManager.RegisterListener( self )
		self.display = display

		self.position = 0
		self.menuFocus =['Yes','No']

		print "Quit created.."

	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Quit class:"

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, KeyEvent ) and focus == 'Quit': 

			# Check cases of events and react
			if event.typeOf() == 'left':
				if self.position == 1:
					self.position -= 1
				
			if event.typeOf() == 'right':
				if self.position == 0:
					self.position += 1

			# The event manager is updated on the current selection
			self.evManager.setEnterValue(self.menuFocus[self.position])

			# Then finally the current status is displayed
			self.display.drawView('Quit', self.position, 0)

			# And position is reset so it is always set to Yes when Quit is opened
			self.position = 0
