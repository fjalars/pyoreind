from KeyEvent import *
import pygame

class Menu:
	"""..."""
	def __init__(self, evManager, display):
		self.evManager = evManager
		self.evManager.RegisterListener( self )
		self.display = display

		self.position = 0
		self.menuFocus = ['Game','Help','Options','Highscore','Quit']
        print "Menu created..."

	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Menu class:"

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, KeyEvent ) and focus == 'Menu': 

			if event.typeOf() == 'up':
				if self.position > 0:
					self.position -= 1
				
			if event.typeOf() == 'down':
				if self.position < 4:
					self.position += 1

			# Check cases of events and react
			self.evManager.setEnterValue(self.menuFocus[self.position])

			# Then finally 
			self.display.drawView('Menu', 0, self.position)
