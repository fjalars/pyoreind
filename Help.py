from KeyEvent import *
import pygame

class Help:
	"""..."""
	def __init__(self, evManager, display):
		self.evManager = evManager
		self.evManager.RegisterListener( self )
		self.display = display

		print "Help created..."

	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Help class:"

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, KeyEvent ) and focus == 'Help': 
			self.display.drawView('Help', 0, 0)

