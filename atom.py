# import modules
import pygame, sys, os, random
from pygame.locals import *

if not pygame.font: print 'Warning, fonts disabled'
if not pygame.mixer: print 'Warning, sound disabled'

#classes for our game objects


#####################
#  Gameboard class  #
#####################

class Gameboard:

	def __init__(self, x_start, y_start, sq_size, screen, linecolor):
		self.x, self.y, self.size = x_start, y_start, sq_size
		self.x_end, self.y_end = self.x+8*self.size, self.y+8*self.size
		self.screen, self.linecolor = screen, linecolor

	def drawBoard(self):
		## Horizontal lines
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y            ), (self.x_end, self.y            ))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+  self.size), (self.x_end, self.y+  self.size))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+2*self.size), (self.x_end, self.y+2*self.size))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+3*self.size), (self.x_end, self.y+3*self.size))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+4*self.size), (self.x_end, self.y+4*self.size))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+5*self.size), (self.x_end, self.y+5*self.size))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+6*self.size), (self.x_end, self.y+6*self.size))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+7*self.size), (self.x_end, self.y+7*self.size))
		pygame.draw.line(self.screen, self.linecolor, (self.x, self.y+8*self.size), (self.x_end, self.y+8*self.size))

		## Lateral lines
		pygame.draw.line(self.screen, self.linecolor, (self.x		 , self.y), (self.x	       , self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+  self.size, self.y), (self.x+  self.size, self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+2*self.size, self.y), (self.x+2*self.size, self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+3*self.size, self.y), (self.x+3*self.size, self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+4*self.size, self.y), (self.x+4*self.size, self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+5*self.size, self.y), (self.x+5*self.size, self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+6*self.size, self.y), (self.x+6*self.size, self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+7*self.size, self.y), (self.x+7*self.size, self.y_end))
		pygame.draw.line(self.screen, self.linecolor, (self.x+8*self.size, self.y), (self.x+8*self.size, self.y_end))

	def getAttrib(self):
		return self.x, self.y, self.size, self.linecolor

	def printAttrib(self):
		print 'X-start: ', self.x
		print 'Y-start: ', self.y
		print 'Size: ', self.size
		print 'Linecolor: ', self.linecolor		

########################
#  1. Crosshair class  #
########################

class Crosshair:
	"""moves a clenched fist on the screen, following the mouse"""
	def __init__(self, x_start, y_start, cross_x_start, cross_y_start, size, screen, scoreboard, atoms):
		self.crosshair = pygame.image.load("graphics/crosshair.png").convert_alpha()
		self.raygun = pygame.image.load("graphics/raygun.png").convert_alpha()
		self.shot = pygame.image.load("graphics/shot.png").convert_alpha()
		self.x_start, self.y_start, self.size, self.screen, self.scoreboard = x_start, y_start, size, screen, scoreboard
		self.x_end, self.y_end = self.x_start+8*self.size, self.y_start+8*self.size
		self.x, self.y = cross_x_start, cross_y_start
		DIR = 1
		dir_change = 0
		scan_result = [False,False,False]
		self.atoms = atoms
		self.last_xy = []
		self.next_xy = []
		self.shot_went_through = False
		self.xy_now = []
		self.origin = []
		
	" The self.move() function updates the status of the crosshair based on arrow_key_down event. The variables are only updated."
	" The self.draw() function is later called to realise the movement. "
	" There are four possible events, up, down, left and right."
	def move(self, direction):
		"move the crosshair all around"
		if direction == 'left':
			if self.x == self.x_start and self.y == self.y_end:
				self.x, self.y = self.x_start-self.size,self.y_end-self.size
			elif self.x == self.x_start and self.y == self.y_start-self.size:
				self.x, self.y = self.x_start-self.size,self.y+self.size
			elif self.x == self.x_start-self.size:
				pass
			else:
				self.x -= self.size
	
		if direction == 'right':
			if self.x == 380 and self.y == 370:
				self.x, self.y = 420,330
			elif self.x == 380 and self.y == 10:
				self.x, self.y = 420,50
			elif self.x == 420:
				pass
			else:
				self.x += self.size


		if direction == 'up':
			if self.x == 420 and self.y == 50:
				self.x, self.y = 380,10
			elif self.x == 60 and self.y == 50:
				self.x, self.y = 100,10
			elif self.y == 10:
				pass
			else:
				self.y -= self.size
			

		if direction == 'down':
			if self.x == 60 and self.y == 330:
				self.x, self.y = 100,370
			elif self.x == 420 and self.y == 330:
				self.x, self.y = 380,370
			elif self.y == 370:
				pass
			else:
				self.y += self.size
			
	
	def printLocation(self):
		"Prints out the x,y location at every gven moment. Is useful for developement & debugging."
		print '************************************'
		print 'Current location: ', self.x, self.y

	def scan(self):
		if self.x < self.x_start or self.x >= self.x_end or self.y < self.y_start or self.y >= self.y_end:
			self.__shoot__()
		else:
			self.__locate__()


	def __locate__(self):
		"Scan for a hit or a miss and draw the end location of the shot"
		if self.atoms[0].isAtomFound(self.x, self.y):
			#print 'Found an atom!!!'
			self.scoreboard.count('hit')
		elif self.atoms[1].isAtomFound(self.x, self.y):
			#print 'Found an atom!!!'
			self.scoreboard.count('hit')
		elif self.atoms[2].isAtomFound(self.x, self.y):
			#print 'Found an atom!!!'
			self.scoreboard.count('hit')
		elif self.atoms[3].isAtomFound(self.x, self.y):
			#print 'Found an atom!!!'
			self.scoreboard.count('hit')
		else:
			#print 'No hit...'
			self.scoreboard.count('miss')


	def __shoot__(self):
		"Scan for a hit or a miss and draw the end location of the shot"
		self.scoreboard.count('shot')
		#if pygame.font:
		#	font = pygame.font.Font(None, 20)
		#	text = font.render("Scanning...", 1, (10, 10, 10))
		#	self.screen.blit(text, (500,90))
		#print 'Shot!'
				
		if self.y == self.y_end:
			self.printLocation()
			self.next_xy = [self.x, self.y]
			self.origin = [self.x, self.y]
			DIR = 1
			print 'Shot from y-end'
			self.__scan_next__(DIR)
		
		if self.x == self.x_start-self.size:
			self.printLocation()
			DIR = 2
			self.next_xy = [self.x, self.y]
			self.origin = [self.x, self.y]
			print 'Shot from x-start'
			self.__scan_next__(DIR)

		if self.y == self.y_start-self.size:
			self.printLocation()
			DIR = 3
			self.next_xy = [self.x, self.y]
			self.origin = [self.x, self.y]
			print 'Shot from y-start'
			self.__scan_next__(DIR)

		if self.x == self.x_end:
			self.printLocation()
			DIR = 4
			self.next_xy = [self.x, self.y]
			self.origin = [self.x, self.y]
			print 'Shot from x-end'
			self.__scan_next__(DIR)


		# 1.SCAN #
	def __scan_next__(self,DIR):

		scan_result =[False,False,False]
		self.DIR = DIR
		i = 0 # counter in loops here below
		#DIR = new_DIR # dir_change is -1 0 or 1

		if self.DIR == 1:
			self.last_xy = self.next_xy
			self.next_xy = [self.next_xy[0], self.next_xy[1] - self.size]
			print ''
			print 'Scanning upwards: x=', self.next_xy[0] - self.size, self.next_xy[0], self.next_xy[0] + self.size, ' y=', self.next_xy[1]
			while i<4 :
				if self.atoms[i].isThereAtom(self.next_xy[0] - self.size, self.next_xy[1]):
					scan_result[0] = True
				if self.atoms[i].isThereAtom(self.next_xy[0]		, self.next_xy[1]):
					scan_result[1] = True
				if self.atoms[i].isThereAtom(self.next_xy[0] + self.size, self.next_xy[1]):
					scan_result[2] = True
				i = i + 1
			print 'Result', scan_result

		if self.DIR == 2: 
			self.last_xy = self.next_xy
			self.next_xy = [self.next_xy[0] + self.size, self.next_xy[1]]
			print ''
			print 'Scanning to the right: x=', self.next_xy[0] + self.size , ' y=', self.next_xy[1] - self.size, self.next_xy[1], self.next_xy[1] + self.size
			while i<4 :
				if self.atoms[i].isThereAtom(self.next_xy[0], self.next_xy[1] - self.size):
					scan_result[0] = True
				if self.atoms[i].isThereAtom(self.next_xy[0], self.next_xy[1]):
					scan_result[1] = True
				if self.atoms[i].isThereAtom(self.next_xy[0], self.next_xy[1] + self.size):	
					scan_result[2] = True
				i = i + 1
			print 'Result', scan_result


		if self.DIR == 3:
			self.last_xy = self.next_xy
			self.next_xy = [self.next_xy[0], self.next_xy[1] + self.size]
			print ''
			print 'Scanning downwards: x=', self.next_xy[0]+self.size, self.next_xy[0], self.next_xy[0]-self.size, ' y=', self.next_xy[1] + self.size
			while i<4 :
				if self.atoms[i].isThereAtom(self.next_xy[0] + self.size, self.next_xy[1]):
					scan_result[0] = True
				if self.atoms[i].isThereAtom(self.next_xy[0], self.next_xy[1]):
					scan_result[1] = True
				if self.atoms[i].isThereAtom(self.next_xy[0] - self.size, self.next_xy[1]):
					scan_result[2] = True
				i = i + 1
			print 'Result', scan_result

		if self.DIR == 4:
			self.last_xy = self.next_xy
			self.next_xy = [self.next_xy[0] - self.size, self.next_xy[1]]
			print ''
			print 'Scanning to the left: x=', self.next_xy[0] - self.size , ' y=', self.next_xy[1] + self.size, self.next_xy[1], self.next_xy[1] - self.size
			while i<4 :
				if self.atoms[i].isThereAtom(self.next_xy[0]	, self.next_xy[1] + self.size):
					scan_result[0] = True
				if self.atoms[i].isThereAtom(self.next_xy[0]	, self.next_xy[1]            ):
					scan_result[1] = True
				if self.atoms[i].isThereAtom(self.next_xy[0]	, self.next_xy[1] - self.size): 
					scan_result[2] = True
				i = i + 1
			print 'Reslult', scan_result
		

		# 2.DECIDE ON NEXT STEP #

		if scan_result == [False,False,False]:
			# Are we at the boundary of the gameboard
			if self.next_xy[0] == self.x_end or self.next_xy[0] == self.x_start-self.size or self.next_xy[1] == self.y_end or self.next_xy[1] == self.y_start-self.size:
				#draw_shot
				print 'The shot went through...'
				self.shotWentThrough(True, self.next_xy)
			else:
				print 'Direction: ', DIR
				self.__scan_next__(DIR) # DIR = 0 - no change of direction.
		
		if scan_result == [False,True,False]:
			print 'The shot was swallowed!'
			self.shotWentThrough(False, self.next_xy)


		if scan_result == [True,False,True]:
			print 'The shot bounced back!'
			self.shotWentThrough(True, self.origin)

		if scan_result == [True,False,False] or scan_result == [True,True,False]:

			if self.last_xy == self.origin:

				self.shotWentThrough(True, self.origin)

			else:

				print 'Direction: ', DIR
				print 'The shot turned right!'
				DIR = DIR + 1
				if DIR == 5:
					DIR = 1
				print 'New direction: ', DIR
				self.next_xy = self.last_xy
				self.__scan_next__(DIR) # DIR = 0 - no change of direction.


		if scan_result == [False,False,True] or scan_result == [False,True,True]:

			if self.last_xy == self.origin:

				self.shotWentThrough(True, self.origin)

			else:

				print 'Direction: ', DIR
				print 'The shot turned left!'
				DIR = DIR - 1
				if DIR == 0:
					DIR = 4
				print 'New direction: ', DIR
				self.next_xy = self.last_xy
				self.__scan_next__(DIR) # DIR = 0 - no change of direction.
		
		
	def draw(self):
		if self.x < self.x_start or self.x >= self.x_end or self.y < self.y_start or self.y >= self.y_end:
			#print 'Raygun coordinates: ', self.x, self.y # For debugging purposes
			self.screen.blit(self.raygun, (self.x,self.y))
		else:
			#print 'Crosshair coordinates: ', self.x, self.y # For debugging purposes
			self.screen.blit(self.crosshair, (self.x,self.y))
	def drawShot(self):

		if self.shot_went_through:
			self.screen.blit(self.shot, (self.xy_now[0],self.xy_now[1]))
		else:
			pass


	def shotWentThrough(self,shot_status, xy_now):
		self.shot_went_through = shot_status
		self.xy_now = xy_now


########################
#    2. Atom class     #
########################

class Atom:

	def __init__(self, rand_num, size, screen):
		# This is a dictionary for all the 64 sqares on the gameboard
		self.x_dict = [100, 140, 180, 220, 260, 300, 340, 380]*8
		self.y_dict = [50]*8+[90]*8+[130]*8+[170]*8+[210]*8+[250]*8+[290]*8+[330]*8
		self.size = size
		self.screen = screen
		self.isfound = False

		# A random number is passed to the functionto pick out one squares. 
		# The random numbers uniqueness is guaranteed by the caller because the atoms cannot coexist on the same square.
		self.x_pos = self.x_dict[rand_num-1]
		self.y_pos = self.y_dict[rand_num-1]
		print self.x_pos, self.y_pos

	def drawAtom(self):
		if self.isfound:
			pygame.draw.rect(self.screen, (170,200,230), Rect((self.x_pos+10,self.y_pos+10),(self.size-20,self.size-20)))
		else:
			#pygame.draw.rect(self.screen, (33,55,77), Rect((self.x_pos+10,self.y_pos+10),(self.size-20,self.size-20)), 3)
			pass
	
	def isAtomFound(self, x, y):
	# Method used by the Crosshair locating method to check for atom locations and make it visible!
	# Note the difference between isThereAtom here below and this one.

		if self.x_pos == x and self.y_pos == y:
			self.isfound = True
			return True
		else:
			return False
	
	def isThereAtom(self, x, y):
	# Method used by the Crosshair scanning method to check for atom locations. 
	# Note the difference between isAtomFound and this one.

		if self.x_pos == x and self.y_pos == y:
			return True
		else:
			return False

	def atomFound(self):
		return self.isfound


#########################
#  3. Scoreboard class  #
#########################

class Scoreboard:

	def __init__(self, screen):
		self.counter = 0
		self.screen = screen

	def count(self,type_of_count):
		if type_of_count == 'shot':
			self.counter += 2
		if type_of_count == 'miss':
			self.counter += 5
		if type_of_count == 'hit':
			self.counter += 1
	
	def printScore(self):
		print 'Score: ', self.counter
	
	def displayScore(self):
		if pygame.font:
			font = pygame.font.Font(None, 36)
			#font = pygame.font.SysFont("arial", 36)
			header = font.render("Scoreboard", 1, (200, 200, 200))
			score =  font.render(str(self.counter), 1, (200,180,180))
			self.screen.blit(header, (460,90))
			self.screen.blit(score, (460,120))

###########################
#  4. Screentricks class  #
###########################

class Screentricks:

	def __init__(self, screen):
		self.screen = screen
		self.grey = pygame.image.load("graphics/grey.png")
		self.x, self.y = 0, 0
		self.focus = 'right'

	def fadeGrayOut(self, alpha=20, loop=40):
		self.alpha = alpha
		self.loop = loop
		self.i = 0

		self.grey.set_alpha(self.alpha)

		while self.i < loop:
			self.screen.blit(self.grey, (0,0))
			self.i +=1
			pygame.display.update()
	
	def move(self, direction):

		if direction == 'left':

			pygame.draw.rect(self.screen, (22,22,22), Rect((340,260),(70,30)), 1)
			pygame.draw.rect(self.screen, (230,200,250), Rect((230,260),(70,30)), 1)
			if pygame.font:
				font = pygame.font.Font(None, 14)
				font2 = pygame.font.Font(None, 20)
				#font = pygame.font.SysFont("arial", 14)
				#font2 = pygame.font.SysFont("arial", 20)
				game_message_1 = font2.render("Do you want to play again?", 1, (200,200,20))
				quit_game_button = font.render("Quit game", 1, (230,200,250))
				play_again_button =  font.render("Play again", 1, (22,22,22))
				self.screen.blit(game_message_1, (230, 220))
				self.screen.blit(quit_game_button, (240,270))
				self.screen.blit(play_again_button, (350,270))
			pygame.display.update()
			self.focus = 'left'

		if direction == 'right':

			pygame.draw.rect(self.screen, (22,22,22), Rect((230,260),(70,30)), 1)
			pygame.draw.rect(self.screen, (230,200,250), Rect((340,260),(70,30)), 1)

			if pygame.font:
				font = pygame.font.Font(None, 14)
				font2 = pygame.font.Font(None, 20)
				#font = pygame.font.SysFont("arial", 14)
				#ont2 = pygame.font.SysFont("arial", 20)
				game_message_1 = font2.render("Do you want to play again?", 1, (200,200,20))
				quit_game_button = font.render("Quit game", 1, (22,22,22))
				play_again_button =  font.render("Play again", 1, (230,200,250))
				self.screen.blit(game_message_1, (230, 220))
				self.screen.blit(quit_game_button, (240,270))
				self.screen.blit(play_again_button, (350,270))
			pygame.display.update()

		#draw base
		#draw focus right
			self.focus = 'right'

	
	def do(self, focus):

		if focus == 'right':
			self.play_again = True

		elif focus == 'left':
			self.play_again = False

	
	def playAgainQ(self):

		self.play_again = 0 # not defined
		self.do_not_complete = True

		pygame.draw.rect(self.screen, (33,55,77), Rect((200,200),(250,120)))		# Draw the big box
		pygame.draw.rect(self.screen, (22,22,22), Rect((230,260),(70,30)), 1)		# Draw the left button out-of focus
		pygame.draw.rect(self.screen, (230,200,250), Rect((340,260),(70,30)), 1)	# Draw the right button with focus

		if pygame.font:
			font = pygame.font.Font(None, 14)
			font2 = pygame.font.Font(None, 20)
			#font = pygame.font.SysFont("arial", 14)
			#font2 = pygame.font.SysFont("arial", 20)
			game_message_1 = font2.render("Do you want to play again?", 1, (200,200,20))
			quit_game_button = font.render("Quit game", 1, (22,22,22))
			play_again_button =  font.render("Play again", 1, (230,200,250))
			self.screen.blit(game_message_1, (230, 220))
			self.screen.blit(quit_game_button, (240,270))
			self.screen.blit(play_again_button, (350,270))
		pygame.display.update()

		while self.do_not_complete:
			event = pygame.event.poll()
			
			if event.type == KEYDOWN:
				if event.key == K_LEFT:
					self.move('left')
						
				if event.key == K_RIGHT:
					self.move('right')
				
				if event.key == K_RETURN:
					self.do(self.focus)
					self.do_not_complete = False

			if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
				pygame.quit()
				sys.exit()

		
		return self.play_again
		


#---------------------------------------------------------------#
#								#
#       Initialize everything and rum the bloody thing		#
#								#
#---------------------------------------------------------------#

def main():

	pygame.init()
	
	screen=pygame.display.set_mode((640,480),0,32)
	pygame.display.set_icon(pygame.image.load("graphics/icon.png").convert_alpha()) #Need an icon 32x32 px. # Neet to create logo!
	pygame.display.set_caption('Atom - 2.0')
	#gray = pygame.image.load("grey.png").convert_alpha()

	#Create The Backgound
	background_png = pygame.image.load("graphics/background_1.png").convert_alpha()
	gameboard_background = pygame.image.load("graphics/gameboard_background.png")
	gameboard_background.set_alpha(180)
	#background = pygame.Surface(screen.get_size())
	#background = background.convert()
	#background.fill((30, 30, 30))
	
	
	## Gameboard variables
	x_start,y_start = 100,50	# The offset coordinates for the gameboard
	sq_size = 40			# The size of the sqares on the gameboard
	linecolor = 200, 200, 200	# THe gameboard line color

	## Initialize the atoms staticly for now
	not_unique = True
	run_game = True
	quit = False
	randNum = [random.randint(1, 64), random.randint(1, 64), random.randint(1, 64), random.randint(1, 64)]
	#randNum = [5, random.randint(1, 64), 5, random.randint(1, 64)] # For debugging random number uniqueness
	#print randNum
	

	while not_unique:	
		if randNum[0] == randNum[1]:
			randNum[0] = random.randint(1, 64)
		elif randNum[0] == randNum[2]:
			randNum[0] = random.randint(1, 64)
		elif randNum[0] == randNum[3]:
			randNum[0] = random.randint(1, 64)
		elif randNum[1] == randNum[2]:
			randNum[1] = random.randint(1, 64)
		elif randNum[1] == randNum[3]:
			randNum[1] = random.randint(1, 64)
		elif randNum[2] == randNum[3]:
			randNum[2] = random.randint(1, 64)		
		else:
			not_unique = False

	print randNum
	
	atom_1 = Atom(randNum[0], sq_size, screen)
	atom_2 = Atom(randNum[1], sq_size, screen)
	atom_3 = Atom(randNum[2], sq_size, screen)
	atom_4 = Atom(randNum[3], sq_size, screen)

	atoms = [atom_1, atom_2, atom_3, atom_4]

		# Initiate the game objects
	scoreboard = Scoreboard(screen)
	gameboard = Gameboard(x_start, y_start, sq_size, screen, linecolor)
	crosshair = Crosshair(x_start, y_start, 260, 370, sq_size, screen, scoreboard, atoms)
	screentr = Screentricks(screen)


	#x_move, y_move = xend-4*sq_size,yend  # the variables for movement of things.


	# Key even delay thoughts
	old_k_delay, old_k_interval = pygame.key.get_repeat()
	#print old_k_delay, old_k_interval
	pygame.key.set_repeat(500,30)
	new_k_delay, new_k_interval = pygame.key.get_repeat()
	#print new_k_delay, new_k_interval
	
	# The soul in the monster!

	while run_game:
		event = pygame.event.poll()
		
		# All quit/exit events are handled with this if-statement
		if event.type == QUIT or event.type == KEYDOWN and event.key == K_ESCAPE:
			#pygame.quit()
			#sys.exit()
			run_game = False
			quit = True
		
		# Here all Crosshair() events are handled, after all, the crosshair is the only move-able and action-able thing in the game :)
		if event.type == KEYDOWN:
			if event.key == K_LEFT:
				crosshair.move('left')
					
			if event.key == K_RIGHT:
				crosshair.move('right')			

			if event.key == K_UP:
				crosshair.move('up')

			if event.key == K_DOWN:
				crosshair.move('down') 
			
			if event.key == K_RETURN:
				crosshair.scan()


		# This contains the main actions in the game
		if event:
			
			# The values are printed only for debugging/developing purposis
			#crosshair.printLocation()
							
			# The main action
			#screen.blit(background, (x_start,y_start)).surface.set_alpha(150)	# Repaint the background
			screen.blit(background_png, (0,0))
			screen.blit(gameboard_background, (x_start,y_start))
			gameboard.drawBoard()		# Draws the gameboard again.. and again
			atom_1.drawAtom()
			atom_2.drawAtom()
			atom_3.drawAtom()
			atom_4.drawAtom()
			crosshair.draw()		# Draw the crosshair at the current location
			crosshair.drawShot()
			#scoreboard.printScore()
			scoreboard.displayScore()	# Draws the scoreboard at each moment
			pygame.display.update()		# Display all the drawing and painting
			
			if atom_1.atomFound() and atom_2.atomFound() and atom_3.atomFound() and atom_4.atomFound():

				run_game = False
				game_over = True
				

	
	# Finally the player is asked if he/she wants to play again

	screentr.fadeGrayOut(20,30)
	scoreboard.displayScore()	# Draws the scoreboard at each moment
	play_again = screentr.playAgainQ()

	if play_again:
		main()
	else:
		pygame.quit()
		sys.exit()

	
#this calls the 'main' function when this script is executed
if __name__ == '__main__': main()
