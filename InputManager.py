from TickEvent import *
from GlobalEvent import *
from KeyEvent import *

import pygame 
from pygame.locals import * 

class InputManager:
	"""..."""
	def __init__(self, evManager):
		self.evManager = evManager
		self.evManager.RegisterListener( self )
		print 'InputManager created...'

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		#print ' InputManager was notified'
		if isinstance( event, TickEvent ):
 			# The event queue since last Tick is processed.
			for single_event in pygame.event.get():
				#print 'Got event:', single_event
				event = None
				
				# All keyboard events are translated into two event groups, GlobalEvents and KeyEvents.
				# GlobalEvents are events that can one and only one action wherever the program is at. E.g. keyboard letterbutton 's' for sound on/off
				# KeyEvents are opposit to GlobalEvents have impact on the process running at every given moment. E.g. keyboard arrowbutton 'up' 

				# Global events...
				if single_event.type == QUIT:
					event = GlobalEvent('exit') # GlobalEvent 1 - asks directly if you want to quit
				elif single_event.type == KEYDOWN and single_event.key == K_ESCAPE:
					event = GlobalEvent('backdown') # GlobalEvent 2 - backs down a level, goes back to menu e.g.
				elif single_event.type == KEYDOWN and single_event.key == K_q:
					event = GlobalEvent('quit') # GlobalEvent 1 - asks directly if you want to quit
				elif single_event.type == KEYDOWN and single_event.key == K_f:
					event = GlobalEvent('fullscreen') 
				elif single_event.type == KEYDOWN and single_event.key == K_s:
					event = GlobalEvent('sound')
				# Key events...
				elif single_event.type == KEYDOWN and single_event.key == K_UP:
					event = KeyEvent('up') 
				elif single_event.type == KEYDOWN and single_event.key == K_DOWN:
					event = KeyEvent('down')
				elif single_event.type == KEYDOWN and single_event.key == K_LEFT:
					event = KeyEvent('left')
				elif single_event.type == KEYDOWN and single_event.key == K_RIGHT:
					event = KeyEvent('right')
				elif single_event.type == KEYDOWN and single_event.key == K_RETURN:
					event = KeyEvent('return')

				# Each new event is then sent to the EventManager for proper routing.

				if event: 
					self.evManager.Post( event )
			
	def generateEvent(self, event):
		if event == 'exit' or event == 'backdown' or event == 'quit' or event == 'fullscreen' or event == 'sound':
			event = GlobalEvent(event)
			self.evManager.Post(event)
		else:
			event = KeyEvent(event)
			self.evManager.Post(event)
