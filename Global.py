from GlobalEvent import *
import pygame

class Global:
	"""..."""
	def __init__(self, evManager, display):
		self.evManager = evManager
		self.evManager.RegisterListener( self )
		self.display = display

		print "Global created..."

	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Global class:"


	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, GlobalEvent ): 
			#print ('Global was called: %s ' % event)
			if event.typeOf() == 'fullscreen':
				pygame.display.toggle_fullscreen()

			if event.typeOf() == 'sound':
				self.display.fadeGrayOut() 

