from KeyEvent import *
import pygame

class Highscore:
	"""..."""
	def __init__(self, evManager, display):
		self.evManager = evManager
		self.evManager.RegisterListener( self )

		self.display = display
		print "Highscore created..."

	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Highscore class:"

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, KeyEvent ) and focus == 'Highscore': 
			self.display.drawView('Highscore', 0, 0)
	
