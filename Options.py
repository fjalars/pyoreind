from KeyEvent import *
import pygame

class Options:
	"""..."""
	def __init__(self, evManager, display):
		self.evManager = evManager
		self.evManager.RegisterListener( self )

		self.display = display
		print "Options created..."

	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Options class:"

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, KeyEvent ) and focus == 'Options': 
			self.display.drawView('Options', 0, 0)

