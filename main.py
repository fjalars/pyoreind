import pygame
from EventManager import *
from TickManager import *
from InputManager import *
from Global import *
from Menu import *
from Game import *
from Quit import *
from Help import *
from Options import *
from Highscore import *
from Display import *

def main():
	"""..."""

	print "Main loop starting..."
	pygame.init()

	# Display 
	screen = pygame.display.set_mode([600, 400])
	display = Display(screen)	

	# Events and input
	evMan = EventManager() # Will have to pass a reference to the input manager so we can generate some fake events when focus is shifted
	inputManager = InputManager(evMan)
	ticker = TickManager(evMan)
	globalManager = Global(evMan, display)

	# Logic
	menu = Menu(evMan, display)
	game = Game(evMan, display)
	help = Help(evMan, display)	
	options = Options(evMan, display)	
	highscore = Highscore(evMan, display)
	quit = Quit(evMan, display)

	# Initial event to kickstart the machine.. and drawing the menu
	inputManager.generateEvent('init') 
	ticker.Run()
   	print "Ready to rumble..."

if __name__ == "__main__":
	main()
