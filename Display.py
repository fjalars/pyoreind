import pygame

class Display:
	"""..."""
	def __init__(self, screen):

		# Screen
		self.screen = screen

		# Menu properties
		self.menuBackground = pygame.image.load("data/twirl_wide_800_600.png").convert_alpha()
		self.grey = pygame.image.load("data/grey.png").convert_alpha()
		self.menuFocus = ''
		self.menu_x = ''
		self.menu_y = ''
		print "Display created..."
		self.linecolor = 200, 200, 200
		self.x = 235
		self.y = 20
		self.width = 0
		self.x_width = 2
		self.y_width = 10
		self.next_x = 40 # Increment for the next button

		# Game properties


	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Display class:"

	#----------------------------------------------------------------------			
	def drawView(self, focus, x, y, update='yes'):
		
		self.menuFocus = focus
		self.menu_x = x
		self.menu_y = y
		self.update = update
		
		# Crear the stage
		self.screen.fill((0,0,0), rect=None, special_flags=0)

		# Stick the background
		self.screen.blit(self.menuBackground, (0,0))
		
		# Call the proper view and draw it.
		if self.menuFocus == 'Menu': 
			self.drawMenu()
			#continue 					
		if self.menuFocus == 'Game': 
			self.drawGame()					
			#continue
		if self.menuFocus == 'Help': 
			self.drawHelp()
			#continue			
		if self.menuFocus == 'Options': 
			self.drawOptions()
			#continue
		if self.menuFocus == 'Highscore': 
			self.drawHighscore()
			#continue
		if self.menuFocus == 'Quit': 
			self.drawQuit()
			#continue

		# Finally update the screen		
		if self.update == 'yes':
			pygame.display.update()
	

	#----------------------------------------------------------------------			
	def drawMenu(self):
	
		# Draw the visibles
		if pygame.font:
			font = pygame.font.Font(None, 36)
			button_1 = font.render("Game", 1, (200, 200, 200))
			button_2 = font.render("Help", 1, (200, 200, 200))
			button_3 = font.render("Options", 1, (200, 200, 200))
			button_4 = font.render("Highscore", 1, (200, 200, 200))
			button_5 = font.render("Quit", 1, (200, 200, 200))

			self.screen.blit(button_1, (235,90))
			self.screen.blit(button_2, (235,90+self.next_x))
			self.screen.blit(button_3, (235,90+2*self.next_x))
			self.screen.blit(button_4, (235,90+3*+self.next_x))								
			self.screen.blit(button_5, (235,90+4*+self.next_x))										

		# The box around currently selected text
		pygame.draw.rect(self.screen, self.linecolor, (230, 85 + self.menu_y*self.next_x, 130, 35), 1) 	#(x,y,x-withd,y-width),width
		

	#----------------------------------------------------------------------			
	def drawGame(self):
	
			# Draw the visibles
			if pygame.font:
				font = pygame.font.Font(None, 36)
				message = font.render("This is the Game view...", 1, (200, 200, 200))
				self.screen.blit(message, (100,100))


	#----------------------------------------------------------------------			
	def drawHelp(self):
	
			# Draw the visibles
			if pygame.font:
				font = pygame.font.Font(None, 36)
				message = font.render("This is the Help view...", 1, (200, 200, 200))
				self.screen.blit(message, (100,100))


	#----------------------------------------------------------------------			
	def drawOptions(self):
	
			# Draw the visibles
			if pygame.font:
				font = pygame.font.Font(None, 36)
				message = font.render("This is the Options view...", 1, (200, 200, 200))
				self.screen.blit(message, (100,100))
		

	#----------------------------------------------------------------------			
	def drawHighscore(self):
	
			# Draw the visibles
			if pygame.font:
				font = pygame.font.Font(None, 36)
				message = font.render("This is the Highscore view...", 1, (200, 200, 200))
				self.screen.blit(message, (100,100))
		
		
	#----------------------------------------------------------------------			
	def drawQuit(self):
	
		if pygame.font:
			font = pygame.font.Font(None, 36)
			#font = pygame.font.SysFont("arial", 36)
			message = font.render("Quit the game?", 1, (200, 200, 200))
			button_1 = font.render("Yes", 1, (200, 200, 200))
			button_2 = font.render("No", 1, (200, 200, 200))
			self.screen.blit(message,  (235,120))
			self.screen.blit(button_1, (260,160))
			self.screen.blit(button_2, (350,160))

		# The box around currently selected text
		pygame.draw.rect(self.screen, self.linecolor, (230+self.menu_x*100, 155, 100, 35), 1) #(x,y,x-withd,y-width),width
	
	#----------------------------------------------------------------------			
	def fadeGrayOut(self, alpha=20, loop=40):
		self.alpha_1 = 0
		self.alpha_2 = 100
		self.alpha_3 = 255
		self.loop = 30
		self.i = 0
		font = pygame.font.Font(None, 36)

		while self.i < self.loop:
			self.drawView(self.menuFocus, self.menu_x, self.menu_y, 'no')
			#self.loop_mess = font.render(str(self.i), 1, (200, 200, 200)).convert_alpha()
			#self.loop_mess.set_alpha(self.i)
			#self.screen.blit(self.loop_mess,  (500,200))
			self.grey.set_alpha(self.i)
			self.screen.blit(self.grey,  (500,300))			
			self.i +=1
			pygame.display.update()


