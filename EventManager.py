from TickEvent import *
from GlobalEvent import *
from KeyEvent import *
from Quit import *
from Menu import *
import pygame

class EventManager:
	"""this object is responsible for coordinating most communication
	between the Model, View, and Controller."""
	def __init__(self):
		from weakref import WeakKeyDictionary
		self.listeners = WeakKeyDictionary()
		self.eventQueue= []
		self.listenersToAdd = []
		self.lastFocus = ''
		self.currentFocus = 'Menu' # Types of views: Menu, Game, Quit, Highscore
		self.enterValue = ''
		self.evC = 0
		self.lastEvent = ''
		self.currentEvent = 'Menu'
		print "EventManager created..."

	#----------------------------------------------------------------------
	def RegisterListener( self, listener ):
		self.listenersToAdd.append(listener)

	#----------------------------------------------------------------------
	def ActuallyUpdateListeners(self):
		for listener in self.listenersToAdd:
			self.listeners[ listener ] = 1

	#----------------------------------------------------------------------
	def ManageFocus(self,event):

		if isinstance(event, GlobalEvent):
			#print ""
			#print "EventManager > ManageFocus called #############################################"
			# Printing current focus and the last one
			print('EventManager > Current focus is: %s') % self.currentFocus
			print('EventManager > Last focus was: %s') % self.lastFocus
		   
		    # Next checking for the event types 'backdown' and 'quit'			
			if event.typeOf() == 'backdown':
				if self.currentFocus == 'Menu' or self.currentFocus == 'Menu_ext':
					self.setFocus('Quit')
				elif self.currentFocus == 'Quit':
					# revert to the last focus
					self.setFocus(self.lastFocus)
					
				elif self.currentFocus == 'Game':
					print('EventManager > Setting currentFocus to Menu with a twist')
					self.setFocus('Menu') # Use this while Menu_ext has not been implemented
					#self.setFocus('Menu_ext') # The menu extended has an extra buttion for returning to a active game from the menu
				else:
					self.setFocus('Menu')

			if event.typeOf() == 'quit':
				if self.currentFocus != 'Quit':
					self.setFocus('Quit')
		
		if isinstance(event, KeyEvent):
			
			if event.typeOf() == 'return':
				print "EventManager > ## Return pressed ##"
				if self.currentFocus == 'Menu':
						self.setFocus(self.enterValue)

				if self.currentFocus == 'Quit':

					# Revert to the last focus
					if self.enterValue == 'No':
						self.generateEvent('backdown','Global')				

					# Exit the game
					elif self.enterValue == 'Yes':
						self.generateEvent('exit','Global')				
			
			

	#----------------------------------------------------------------------
	def setFocus( self, focus ):

		#print ""
		#print "EventManager > ** setFocus called **"
		#print('EventManager > Setting currentFocus to %s') % focus
		#print('EventManager > Setting lastFocus to %s') % self.currentFocus
		#print('EventManager > Finally generating an focus event')
		self.lastFocus = self.currentFocus
		self.currentFocus = focus
		# Virtual event is then generated to trigger display activity.
		self.generateEvent('focus event','Key')
	
	#----------------------------------------------------------------------
	def setEnterValue( self, enterValue ):
		#print ""
		#print "EventManager > ** setEnterValue called **"
		# The nextFocus variable is used for the menu system.
		#print('EventManager > Current Focus is :: %s') % self.currentFocus
		#print('EventManager > Current enterValue is:: %s') % self.enterValue
		self.enterValue = enterValue
		#print('EventManager > New enterValue is:: %s') % self.enterValue
		


	#----------------------------------------------------------------------
	def generateEvent(self, eventToGen, eventType):
		#print ""
		#print "EventManager > ** generateEvent called **"
		#print "EventManager > Event type: %s" % eventType
		#print "EventManager > Event: %s" % eventToGen
		if eventType == 'Global':
			event = GlobalEvent(eventToGen)
	
		elif eventType == 'Key':	
			event = KeyEvent(eventToGen)
		
		self.Post(event)
	
	
	
	#----------------------------------------------------------------------
	def Post( self, event ):

		self.eventQueue.append(event)
		self.ManageFocus(event)
		if isinstance(event, TickEvent):
			# Consume the event queue every Tick.
			if self.listenersToAdd:
				self.ActuallyUpdateListeners()
				self.ConsumeEventQueue()
		
		else:
			pass
			#print 'EventManager: TickEvent not posted.. bug?'
			#Debug( "     Message: " + event.name )

	#----------------------------------------------------------------------
	def ConsumeEventQueue(self):
		self.evC += 1 # Event counter ... just for fun :)
		i = 0
		while i < len( self.eventQueue ):
			event = self.eventQueue[i]
			for listener in self.listeners:
				old = len(self.eventQueue)
				listener.Notify( event,self.currentFocus )
			i += 1

			# If there are new listeners to add
			if self.listenersToAdd:
				self.ActuallyUpdateListeners()

		self.eventQueue= []
		
