from TickEvent import *
from GlobalEvent import *

class TickManager:
	"""..."""
	def __init__(self, evManager):
		self.evManager = evManager
		self.evManager.RegisterListener( self )
		print 'TickManager created...'

		self.keepGoing = 1

	#----------------------------------------------------------------------
	def Run(self):
		while self.keepGoing:
			event = TickEvent()
			# TODO: Have to fix the TickEvent() so it creates a delay before returning.
			self.evManager.Post( event )

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, GlobalEvent ) and event.typeOf() == 'exit': 
			self.keepGoing = False
			print 'Quitting...Bye!'

