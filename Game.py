from KeyEvent import *
import pygame

class Game:
	"""..."""
	def __init__(self, evManager, display):
		self.evManager = evManager
		self.evManager.RegisterListener( self )
		self.display = display

		print "Game created..."

	#----------------------------------------------------------------------		
	def __str__(self):
		return "This is the Game class:"

	#----------------------------------------------------------------------
	def Notify(self, event, focus):
		if isinstance( event, KeyEvent ) and focus == 'Game': 
			self.display.drawView('Game', 0, 0)
